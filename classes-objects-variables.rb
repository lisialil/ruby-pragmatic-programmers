require './song.rb'

song = Song.new("Bicylops", "Fleck", 260) # calles initialize
puts song
puts song.to_s
puts song.inspect
puts


###### Extend a class

class KaraokeSong < Song # < means is a subclass of
  def initialize(name, artist, duration, lyrics)
    super(name, artist, duration)
    @lyrics = lyrics
  end

  def to_s
    super + " [#{@lyrics}]"
  end
end

aSong = KaraokeSong.new("My Way", "Sinatra", 225, "And now, the...")
puts aSong
puts


###### Getters and setters
puts song.artist
song.duration = 777
puts song.duration


###### Class variables

s1 = Song.new("Song1", "Artist1", 234)  # test songs..
s2 = Song.new("Song2", "Artist2", 345)
puts s1.play
puts s2.play
puts s2.play
puts s1.play
puts


###### Class methods

song1 = Song.new("Bicylops", "Fleck", 260)
puts SongList.isTooLong(song1)
song2 = Song.new("The Calling", "Santana", 468)
puts SongList.isTooLong(song2)


###### Singletons

class Logger
  private_class_method :new # this prevents anyone from creating a logger object using the conventional constructor
  @@logger = nil
  def Logger.create
    @@logger = new unless @@logger
    @@logger # return @@logger
  end
end


###### Other constructors

class Shape
  def initialize(numSides, perimeter)
    puts "sides #{numSides}, perimeter #{perimeter}"
  end
  def Shape.triangle(sideLength)
    Shape.new(3, sideLength*3) # calls initialize
  end
  def Shape.square(sideLength)
    Shape.new(4, sideLength*4)
  end
end

puts


###### Variables
person1 = "Tim"
person2 = person1
person2[0] = "J"
puts person1
puts person2

# person1.freeze
# person1[2] = "l" # raises FrozenError
