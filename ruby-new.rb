#!/usr/local/bin/ruby -w

###### Arrays

arr1 = [0, "hello", 6.7]
arr2 = %w{ ant bee cat dog elk }
arr3 = Array.new

puts arr1
puts
puts arr2[0]
puts arr3.length
puts


###### Hashes

instSection = {
  'cello'     => 'string',
  'clarinet'  => 'woodwind',
  'drum'      => 'percussion'
}

puts instSection['drum']
puts instSection['octopus']
puts

histogram = Hash.new(0)
puts histogram['key1']
histogram['key1'] = histogram['key1'] + 1
puts histogram['key1']


###### If, while

count = 13
if count > 10
  puts "Try again"
elsif tries == 3
  puts "You lose"
else
  puts "Enter a number"
end
puts

# If the body of if/while statement is only one statement, can use one line:
square = 10
square = square*square  while square < 1000


###### Regular expressions
# /expression/

# match operator =~
line = "Python was my first programming language. I don't know Perl."
if line =~ /P(erl|ython)/
  puts "Scripting language mentioned: #{line}"
end

puts line.sub(/Perl/, 'Ruby')    # replace first 'Perl' with 'Ruby'
puts line.gsub(/Python/, 'Ruby') # replace every 'Python' with 'Ruby'
puts


###### Blocks and iterators

def callBlock1
  yield
  yield
end

callBlock1 { puts "In the block" }
callBlock1 do puts "Also in the block" end

# yield can have arguments used in the block
def callBlock2
  yield("John", 2)
end

callBlock2 { |name, age| puts "#{name} is #{age} years old" }
puts


###### I/O

printf "Number: %5.2f, String: %s", 1.23, "hello"

#line = gets
#puts line
