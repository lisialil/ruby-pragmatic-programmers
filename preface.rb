#!/usr/local/bin/ruby -w

puts "Hello world!"


def sayGoodnight(name)
  result = "Goodnight, " + name
  return result
end

# Time for bed...
puts sayGoodnight("John-Boy")
puts(sayGoodnight("Mary-Ellen"))
puts(sayGoodnight "Erin")


def sayGoodMorning(name)
  "Good morning, #{name}" # expression interpolation
end

puts sayGoodMorning("Jason")
