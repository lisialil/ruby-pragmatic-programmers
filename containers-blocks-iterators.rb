###### Arrays

arr = Array.new
puts arr.length
arr[0] = "oooh"
arr[1] = "aaah"
puts arr.length

puts arr[-1]
puts arr[-99] == nil
puts

a = [1, 3, 5, 7, 9]
puts a[1,3] # start at 1, count 3
puts
puts a[1..3] # start at 1, stop at 3 inclusive
puts
puts a[1...3] # start at 1, stop at 3 exclusive
puts

a[11] = 77
puts a.to_s
puts

###### Hashes

class Song
  attr_accessor :title, :artist, :num

  def initialize(title, artist, num)
    @title = title
    @artist = artist
    @num = num
  end

  def print
    puts "Title: #{@title}. Artist: #{@artist}. Num: #{@num}"
  end
end

class SongList
  def initialize
    @songs = Array.new
  end

  def print
    @songs.each do |song|
      puts song.print
    end
  end

  def append(song)
    @songs << song
    self
  end

  def deleteFirst
    @songs.shift
  end

  def deleteLast
    @songs.pop
  end

  def[](key)
    if key.kind_of?(Integer)
      return @songs[key]
    else
      return @songs.find { |aSong| key == aSong.title }
    end
  end
end

songlist = SongList.new
songlist.
  append(Song.new('title1', 'artist1', 1)).
  append(Song.new('title2', 'artist2', 2)).
  append(Song.new('title3', 'artist3', 3)).
  append(Song.new('title4', 'artist4', 4))

songlist.print

puts songlist.deleteFirst.title
puts songlist.deleteLast.title
puts songlist[1].title
puts songlist[11] == nil
songlist["title2"].print
puts songlist["some other title"] == nil
