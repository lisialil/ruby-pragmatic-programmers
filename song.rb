class Song
  attr_accessor :name, :artist, :duration

  @@plays = 0
  def initialize(name, artist, duration)
    @name     = name # @name is instance variable
    @artist   = artist
    @duration = duration
    @plays = 0
  end

  def to_s
    "Song: #{@name}--#{@artist} (#{@duration})" # #{var-name} substitutes the variable
  end

  # virtual attributes: get and set attributes in different formats
  def durationInMinutes
    @duration/60.0   # force floating point
  end

  def durationInMinutes=(value)
    @duration = (value*60).to_i
  end

  def play
    @plays += 1
    @@plays += 1
    "This  song: #@plays plays. Total #@@plays plays."
  end
end


class WordIndex
  def initialize
    @index = Hash.new(nil)
  end

  def index(obj, *phrases)
    phrases.each do |aPhrase|
      aPhrase.scan /\w[-\w']+/ do |aWord| # String.scan returns an array containing all the matches
        aWord.downcase!
        @index[aWord] = [] if @index[aWord].nil?
        @index[aWord].push(obj)
      end
    end
  end

  def lookup(aWord)
    @index[aWord.downcase]
  end
end


class SongList
  attr_accessor :songs

  def initialize
    @songs = Array.new
    @index = WordIndex.new
  end

  def append(song)
    @songs << song
    @index.index(song, song.name, song.artist)
    self
  end

  def deleteFirst
    @songs.shift
  end

  def deleteLast
    @songs.pop
  end

  def[](key)
    if key.kind_of?(Integer)
      return @songs[key]
    else
      return @songs.find { |aSong| key == aSong.title }
    end
  end

  MaxTime = 5*60
  def SongList.isTooLong(aSong) # class method
    return aSong.duration > MaxTime
  end

  def lookup(aWord)
    @index.lookup(aWord)
  end
end
