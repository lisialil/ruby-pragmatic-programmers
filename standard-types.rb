require './song.rb'

song = Song.new("Bicylops", "Fleck", 260) # calles initialize
puts song

###### Strings

aString = "/jazz/j00319.mp3  | 2:58 | Louis    Armstrong  | Wonderful World\n"
print aString, "********\n"
aString = aString.chomp
print aString, "*****\n"

file, length, artist, name = aString.split(/\s*\|\s*/)
puts file
puts length
puts name
puts artist

artist = artist.squeeze
puts artist

mins, secs = length.scan(/\d+/)
puts mins
puts secs
duration = mins.to_i * 60 + secs.to_i
puts duration

songs = SongList.new

songs.append Song.new(name, artist, duration)

puts songs[0]
puts

def process_song(str)
  str = str.chomp
  file, length, artist, name = str.split(/\s*\|\s*/)
  artist = artist.squeeze
  mins, secs = length.scan(/\d+/)
  duration = mins.to_i * 60 + secs.to_i
  return name, artist, duration
end

songs.append Song.new(*process_song("/jazz/j00132.mp3  | 3:45 | Fats     Waller     | Ain't Misbehavin'\n"))
songs.append Song.new(*process_song("/bgrass/bg0732.mp3| 4:09 | Strength in Numbers | Texas Red\n"))
songs.append Song.new("Wonderful Tonight", "Eric Clapton", 223)

puts songs.songs
puts

puts songs.lookup("ain't")
puts songs.lookup("RED")
puts songs.lookup("WoRlD")
puts
puts "wonderful"
puts songs.lookup("wonderful")
puts


###### Ranges

digits = (0..9)
print digits.reject {|i| i < 5 }
digits.each { |digit| puts digit * digit }
puts

class VU
  include Comparable
  attr :volume

  def initialize(volume)
    @volume = volume
  end

  def inspect
    '#' * @volume
  end

  def <=>(other)
    self.volume <=> other.volume
  end

  def succ
    raise(IndexError, "Volume too big") if @volume >= 9
    VU.new(@volume.succ)
  end
end

medium = VU.new(4)..VU.new(7)
print medium.to_a
puts
puts medium.include?(VU.new(3))
puts (1..9) === 5 # interval test: does the value fall within the interval?
puts (1..9) === 15
puts


###### Regular expressions

a = Regexp.new('^\s*[a-z]')
b = /^\s*[a-z]/
c = %r{^\s*[a-z]}

puts a == b
puts b == c

a = "Fats Waller"
puts a =~ /a/
puts /a/ =~ a
puts $&
  puts $`
puts $'
puts

a = "Oh, this is th\nthe time th"
puts a =~ /th/ # 4
puts a =~ /^th/ # start of a line 15
puts a =~ /th$/ # end of a line 12
puts a =~ /\Ath/ # start of string no match
puts a =~ /th\z/ # end of string 24
puts

b = "aisa ais isa"
puts b =~ /is/ # 1
puts b =~ /is\b/ # word boundary 6
puts b =~ /\bis/ # word boundary 9
puts

c = "isa ais aisa"
puts c =~ /\Bis/ # nonword boundary 5
